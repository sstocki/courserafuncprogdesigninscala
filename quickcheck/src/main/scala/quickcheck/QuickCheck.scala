package quickcheck

import org.scalacheck.Prop._
import org.scalacheck._

import scala.annotation.tailrec

abstract class QuickCheckHeap extends Properties("Heap") with IntHeap {

  lazy val genHeap: Gen[H] = for {
    e: A <- Arbitrary.arbitrary[A]
    h <- Gen.oneOf(Gen.const(empty), genHeap)
  } yield insert(e, h)

  implicit lazy val arbHeap: Arbitrary[H] = Arbitrary(genHeap)

  property("gen1") = forAll { (h: H) =>
    val m = if (isEmpty(h)) 0 else findMin(h)
    findMin(insert(m, h)) == m
  }

  property("min1") = forAll { a: Int =>
    val h: H = insert(a, empty)
    findMin(h) == a
  }

  property("min2") = forAll { (a: Int, b: Int) =>
    val h: H = insert(a, insert(b, empty))
    findMin(h) == Math.min(a, b)
  }

  property("min1Delete") = forAll { a: Int =>
    val h: H = deleteMin(insert(a, empty))
    isEmpty(h) == true
  }

  property("sorted") = forAll { (h: H) =>

    @tailrec
    def iterate(temp: H, seq: Seq[A]): Seq[A] = {
      if(isEmpty(temp)) seq
      else iterate(deleteMin(temp), seq :+ findMin(temp))
    }

    val s: Seq[A] = iterate(h, Seq())
    s == s.sorted
  }

  property("minMeld") = forAll { (h1: H, h2: H) =>
    val h: H = meld(h1, h2)
    findMin(h) == Math.min(findMin(h1), findMin(h2))
  }

  // my checks

  property("meldWithEmpty") = forAll { (h1: H) =>
    val h: H = meld(h1, empty)
    findMin(h) == findMin(h1)
  }

  property("deleteMin2Sorted") = forAll { (a: Int, b: Int) =>
    val h: H = insert(a, insert(b, empty))
    findMin(deleteMin(h)) == Math.max(a, b)
  }

  property("deleteMin3Sorted") = forAll { (a: Int, b: Int, c: Int) =>
    val h: H = insert(a, insert(b, insert(c, empty)))

//    val deleteMin1 = deleteMin(h)
//    val deleteMin2 = deleteMin(deleteMin1)
//    val min = findMin(deleteMin2)
//    val max = Seq(a,b,c).max
//
//    if(min != max) {
//      println(s"h=${h}")
//      println(s"deleteMin1=${deleteMin1}")
//      println(s"deleteMin2=${deleteMin2}")
//      println(s"min=${min}; max=${max}")
//    }

    findMin(deleteMin(deleteMin(h))) == Math.max(Math.max(a, b), c)
  }
}
