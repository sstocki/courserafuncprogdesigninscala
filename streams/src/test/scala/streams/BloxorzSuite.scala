package streams

import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class BloxorzSuite extends FunSuite {

  /**
    * Level - passcode
    *
    * Level 01 - code : 780464
    * Level 02 - code : 290299
    * Level 03 - code : 918660
    * Level 04 - code : 520967
    * Level 05 - code : 028431
    * Level 06 - code : 524383
    * Level 07 - code : 189493
    * Level 08 - code : 499707
    * Level 09 - code : 074355
    * Level 10 - code : 300590
    * Level 11 - code : 291709
    * Level 12 - code : 958640
    * Level 13 - code : 448106
    * Level 14 - code : 210362
    * Level 15 - code : 098598
    * Level 16 - code : 000241
    * Level 17 - code : 683596
    * Level 18 - code : 284933
    * Level 19 - code : 119785
    * Level 20 - code : 543019
    * Level 21 - code : 728724
    * Level 22 - code : 987319
    * Level 23 - code : 293486
    * Level 24 - code : 088198
    * Level 25 - code : 250453
    * Level 26 - code : 426329
    * Level 27 - code : 660141
    * Level 28 - code : 769721
    * Level 29 - code : 691859
    * Level 30 - code : 280351
    * Level 31 - code : 138620
    * Level 32 - code : 879021
    * Level 33 - code : 614955
    */

  trait SolutionChecker extends GameDef with Solver with StringParserTerrain {
    /**
      * This method applies a list of moves `ls` to the block at position
      * `startPos`. This can be used to verify if a certain list of moves
      * is a valid solution, i.e. leads to the goal.
      */
    def solve(ls: List[Move]): Block =
      ls.foldLeft(startBlock) { case (block, move) => move match {
        case Left => block.left
        case Right => block.right
        case Up => block.up
        case Down => block.down
      }
      }
  }

  trait Level1 extends SolutionChecker {
    /* terrain for level 1*/

    val level =
      """ooo-------
        |oSoooo----
        |ooooooooo-
        |-ooooooooo
        |-----ooToo
        |------ooo-""".stripMargin

    val optsolution = List(Right, Right, Down, Right, Right, Right, Down)
  }

  // passcode = 524383
  trait Level6 extends SolutionChecker {
    /* terrain for level 6*/

    val level =
      """-----oooooo----
        |-----o--ooo----
        |-----o--ooooo--
        |Sooooo-----oooo
        |----ooo----ooTo
        |----ooo-----ooo
        |------o--oo----
        |------ooooo----
        |------ooooo----
        |-------ooo-----""".stripMargin

    val optsolution = List()//List(Right, Right, Down, Right, Right, Right, Down)
  }

  trait LevelUnsolvable extends SolutionChecker {
    /* terrain for unsolvable level*/

    val level =
      """ooo-------
        |oSoooo----
        |ooooooooo-
        |-ooooooooo
        |-----ooooo
        |----T-ooo-""".stripMargin

    val optsolution = List()
  }


  test("terrain function level 1") {
    new Level1 {
      assert(terrain(Pos(0, 0)), "0,0")
      assert(terrain(Pos(1, 1)), "1,1") // start
      assert(terrain(Pos(4, 7)), "4,7") // goal
      assert(terrain(Pos(5, 8)), "5,8")
      assert(!terrain(Pos(5, 9)), "5,9")
      assert(terrain(Pos(4, 9)), "4,9")
      assert(!terrain(Pos(6, 8)), "6,8")
      assert(!terrain(Pos(4, 11)), "4,11")
      assert(!terrain(Pos(-1, 0)), "-1,0")
      assert(!terrain(Pos(0, -1)), "0,-1")
    }
  }

  test("findChar S level 1") {
    new Level1 {
      assert(startPos == Pos(1, 1))
    }
  }

  test("findChar T level 1") {
    new Level1 {
      assert(goal == Pos(4, 7))
    }
  }

  test("startBlock level 1") {
    new Level1 {
      assert(startBlock == Block(Pos(1, 1), Pos(1, 1)))
    }
  }

  test("isStanding level 1") {
    new Level1 {
      assert(Block(Pos(1, 1), Pos(1, 1)).isStanding)
      assert(!Block(Pos(1, 1), Pos(1, 2)).isStanding)
    }
  }

  test("neighbors start position level 1") {
    new Level1 {
      assert(Block(Pos(1, 1), Pos(1, 1)).neighbors ==
        List(
          (Block(Pos(1, -1), Pos(1, 0)), Left),
          (Block(Pos(1, 2), Pos(1, 3)), Right),
          (Block(Pos(-1, 1), Pos(0, 1)), Up),
          (Block(Pos(2, 1), Pos(3, 1)), Down)
        )
      )
    }
  }

  test("legalNeighbors start position level 1") {
    new Level1 {
      assert(Block(Pos(1, 1), Pos(1, 1)).legalNeighbors ==
        List(
          (Block(Pos(1, 2), Pos(1, 3)), Right),
          (Block(Pos(2, 1), Pos(3, 1)), Down)
        )
      )
    }
  }

  test("isLegal level 1") {
    new Level1 {
      assert(Block(Pos(1, 1), Pos(1, 1)).isLegal)
      assert(Block(Pos(1, 2), Pos(2, 2)).isLegal)
      assert(!Block(Pos(0, 2), Pos(0, 3)).isLegal)
      assert(!Block(Pos(0, 3), Pos(0, 3)).isLegal)
      assert(!Block(Pos(2, 0), Pos(3, 0)).isLegal)
      assert(!Block(Pos(3, 0), Pos(3, 0)).isLegal)
      assert(!Block(Pos(1, -1), Pos(1, 0)).isLegal)
      assert(!Block(Pos(-1, 1), Pos(0, 1)).isLegal)
    }
  }

  test("done level 1") {
    new Level1 {
      assert(!done(Block(Pos(1, 1), Pos(1, 1))))
      assert(done(Block(Pos(4, 7), Pos(4, 7))))
    }
  }

  test("neighborsWithHistory example") {
    new Level1 {
      val result = neighborsWithHistory(
        Block(Pos(1, 1), Pos(1, 1)),
        List(Left, Up)
      )
      val expectedResult = Set(
        (Block(Pos(1, 2), Pos(1, 3)), List(Right, Left, Up)),
        (Block(Pos(2, 1), Pos(3, 1)), List(Down, Left, Up))
      ).toStream

      assert(result == expectedResult)
    }
  }

  test("newNeighborsOnly example") {
    new Level1 {
      val result = newNeighborsOnly(
        Set(
          (Block(Pos(1, 2), Pos(1, 3)), List(Right, Left, Up)),
          (Block(Pos(2, 1), Pos(3, 1)), List(Down, Left, Up))
        ).toStream,
        Set(
          Block(Pos(1, 2), Pos(1, 3)),
          Block(Pos(1, 1), Pos(1, 1))
        )
      )
      val expectedResult = Set(
        (Block(Pos(2, 1), Pos(3, 1)), List(Down, Left, Up))
      ).toStream

      assert(result == expectedResult)
    }
  }

  test("from Start with empty explored level 1") {
    new Level1 {
      val result: Stream[(Block, List[Move])] = from(Stream((Block(Pos(1, 1), Pos(1, 1)), List.empty)), Set())
      val expected0 = (Block(Pos(3, 9), Pos(3, 9)), List(Right, Up, Right, Down, Right, Right, Down, Right, Right))
      val expected1 = (Block(Pos(4, 8), Pos(4, 9)), List(Right, Down, Right, Right, Right, Down, Right, Right))
      val expected2 = (Block(Pos(4, 9), Pos(4, 9)), List(Right, Right, Down, Right, Right, Down, Right, Right))
      assert(result.toList(0) == expected0)
      assert(result.toList(1) == expected1)
      assert(result.toList(2) == expected2)
    }
  }

  test("pathsFromStart level 1") {
    new Level1 {
      val result: Stream[(Block, List[Move])] = pathsFromStart
      val expected0 = (Block(Pos(3, 9), Pos(3, 9)), List(Right, Up, Right, Down, Right, Right, Down, Right, Right))
      val expected1 = (Block(Pos(4, 8), Pos(4, 9)), List(Right, Down, Right, Right, Right, Down, Right, Right))
      val expected2 = (Block(Pos(4, 9), Pos(4, 9)), List(Right, Right, Down, Right, Right, Down, Right, Right))
      assert(result.toList(0) == expected0)
      assert(result.toList(1) == expected1)
      assert(result.toList(2) == expected2)
    }
  }

  test("pathsToGoal level 1") {
    new Level1 {
      val result: Stream[(Block, List[Move])] = pathsToGoal
      val expectedResult = List(
        (Block(Pos(4, 7), Pos(4, 7)), List(Down, Right, Right, Right, Down, Right, Right)),
        (Block(Pos(4, 7), Pos(4, 7)), List(Right, Down, Down, Right, Right, Down, Right)),
        (Block(Pos(4, 7), Pos(4, 7)), List(Right, Down, Right, Right, Down, Down, Right))
      )
      assert(result.toList == expectedResult)
    }
  }

  test("optimal solution for level 1") {
    new Level1 {
      assert(solve(solution) == Block(goal, goal))
    }
  }

  test("optimal solution length for level 1") {
    new Level1 {
      assert(solution.length == optsolution.length)
    }
  }


  test("findChar S level 6") {
    new Level6 {
      assert(startPos == Pos(3, 0))
    }
  }

  test("findChar T level 6") {
    new Level6 {
      assert(goal == Pos(4, 13))
    }
  }

  test("optimal solution for level 6") {
    new Level6 {
      assert(solve(solution) == Block(goal, goal))
    }
  }

  test("optimal solution length for level 6") {
    new Level6 {
      assert(solution.length == optsolution.length)
    }
  }

  test("optimal solution length for unsolvable level") {
    new LevelUnsolvable {
      assert(solution.length == 0)
    }
  }

}
