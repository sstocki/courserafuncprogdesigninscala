package calculator

import calculator.TweetLength.MaxTweetLength
import org.junit.runner.RunWith
import org.scalatest.{FunSuite, _}
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class CalculatorSuite extends FunSuite with ShouldMatchers {

  /** ****************
    * * TWEET LENGTH **
    * *****************/

  def tweetLength(text: String): Int =
    text.codePointCount(0, text.length)

  test("tweetRemainingCharsCount with a constant signal") {
    val result: Signal[Int] = TweetLength.tweetRemainingCharsCount(Var("hello world"))
    assert(result() == MaxTweetLength - tweetLength("hello world"))

    val tooLong = "foo" * 200
    val result2 = TweetLength.tweetRemainingCharsCount(Var(tooLong))
    assert(result2() == MaxTweetLength - tweetLength(tooLong))
  }

  test("tweetRemainingCharsCount with a supplementary char") {
    val result = TweetLength.tweetRemainingCharsCount(Var("foo blabla \uD83D\uDCA9 bar"))
    assert(result() == MaxTweetLength - tweetLength("foo blabla \uD83D\uDCA9 bar"))
  }


  test("colorForRemainingCharsCount with a constant signal") {
    val resultGreen1 = TweetLength.colorForRemainingCharsCount(Var(52))
    assert(resultGreen1() == "green")
    val resultGreen2 = TweetLength.colorForRemainingCharsCount(Var(15))
    assert(resultGreen2() == "green")

    val resultOrange1 = TweetLength.colorForRemainingCharsCount(Var(12))
    assert(resultOrange1() == "orange")
    val resultOrange2 = TweetLength.colorForRemainingCharsCount(Var(0))
    assert(resultOrange2() == "orange")

    val resultRed1 = TweetLength.colorForRemainingCharsCount(Var(-1))
    assert(resultRed1() == "red")
    val resultRed2 = TweetLength.colorForRemainingCharsCount(Var(-5))
    assert(resultRed2() == "red")
  }

  /** ****************
    * * Polynomial **
    * *****************/

  test("Polynomial.computeDelta(1,2,3) < 0") {
    val delta: Signal[Double] = Polynomial.computeDelta(Var(1), Var(2), Var(3))
    assert(delta() == -8)
  }

  test("Polynomial.computeDelta(1,4,4) == 0") {
    val delta: Signal[Double] = Polynomial.computeDelta(Var(1), Var(4), Var(4))
    assert(delta() == 0)
  }

  test("Polynomial.computeDelta(1,-4,3) > 0") {
    val delta = Polynomial.computeDelta(Var(1), Var(-4), Var(3))
    assert(delta() == 4)
  }

  test("Polynomial.computeSolutions(1,2,3,-8) no solutions") {
    val solution: Signal[Set[Double]] = Polynomial.computeSolutions(Var(1), Var(2), Var(3), Var(-8))
    assert(solution() == Set())
  }

  test("Polynomial.computeSolutions(1,4,4,0) one solution") {
    val solution: Signal[Set[Double]] = Polynomial.computeSolutions(Var(1), Var(4), Var(4), Var(0))
    assert(solution() == Set(-2))
  }

  test("Polynomial.computeSolutions(1,-4,3,4) two solutions") {
    val solution = Polynomial.computeSolutions(Var(1), Var(-4), Var(3), Var(4))
    assert(solution() == Set(3, 1))
  }

  /** ****************
    * * Calculator **
    * *****************/

  test("Calculator.eval()") {
    Calculator.computeValues(Map(
      "a" -> Var(Literal(-1)),
      "b" -> Var(Literal(2)),
      "c" -> Var(Literal(-3))
    ))
  }
}
