package calculator

object Polynomial {
  def computeDelta(a: Signal[Double],
                   b: Signal[Double],
                   c: Signal[Double]): Signal[Double] = {
    Signal[Double](b() * b() - 4 * a() * c())
  }

  def computeSolutions(a: Signal[Double],
                       b: Signal[Double],
                       c: Signal[Double],
                       delta: Signal[Double]): Signal[Set[Double]] = {
    if(delta() < 0) Signal[Set[Double]](Set())
    else if(delta() == 0) {
      val solution: Double = -1 * b() / 2 * a()
      Signal[Set[Double]](Set(solution))
    }
    else {
      val solution1: Double = (-1 * b() - Math.sqrt(delta())) / 2 * a()
      val solution2: Double = (-1 * b() + Math.sqrt(delta())) / 2 * a()
      Signal[Set[Double]](Set(solution1, solution2))
    }
  }
}
